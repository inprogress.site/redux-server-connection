// @flow
/* global FormData */

import AuthFactory from "./Auth";
const Auth = AuthFactory();

import httpStatusCodes from "http-status-codes";
import httpHelper from "./helpers/http";
import { stringify } from "qs";

const requested = (type, url) => {
  return {
    type: `REQUEST_${type.toUpperCase()}`,
    url: url.replace(/.*\/\/[^\/]*/, "")
  };
};

const received = (type, url, method, params, data) => {
  const response = {
    data,
    url: url.replace(/.*\/\/[^\/]*/, ""),
    params
  };

  switch (method.toLowerCase()) {
    case "get":
    case "head":
      response.type = `RECEIVE_${type.toUpperCase()}`;
      break;
    default:
      response.type = `RESPONSE_${method.toUpperCase()}_${type.toUpperCase()}`;
      break;
  }
  return response;
};

const request = (url, method, params = {}, headers = {}, type) => {
  if (url === "") {
    return () => {};
  }

  const strParams = stringify(params);
  let body = params;
  const init = { method, credentials: "same-origin" };

  if (typeof FormData === "undefined" || !(params instanceof FormData)) {
    body = JSON.stringify(params);
  }

  if (method !== "GET" && method !== "HEAD") {
    init.body = body;
    if (typeof FormData === "undefined" || !(body instanceof FormData)) {
      headers = Object.assign(headers, {
        Accept: "application/json",
        "Content-Type": "application/json"
      });
    }
  }

  init.headers = headers;
  if (typeof navigator !== "undefined" && navigator.product === "ReactNative") {
    init.headers["x-token"] = Auth.getToken();
  }

  return (dispatch: Function, getState: Function) => {
    let requestUrl;

    if (typeof url === "function") {
      requestUrl = url(getState());
    } else {
      requestUrl = url;
    }

    if (strParams !== "" && (method === "GET" || method === "HEAD")) {
      requestUrl += `?${strParams}`;
    }

    dispatch(requested(type, requestUrl));
    return fetch(requestUrl, init)
      .then(response => {
        if (response.status === httpStatusCodes.UNAUTHORIZED) {
          Auth.logout();
        }

        if (method === "HEAD" || method === "DELETE") {
          return httpHelper.isSuccess(response.status);
        } else if (method === "GET") {
          if (
            response.status === httpStatusCodes.NO_CONTENT ||
            response.status === httpStatusCodes.ACCEPTED
          ) {
            return true;
          } else if (response.status === httpStatusCodes.NOT_FOUND) {
            return false;
          }
        }

        if (!httpHelper.isSuccess(response.status)) {
          throw new Error(response.statusText);
        }

        return response.json();
      })
      .then(json => {
        dispatch(received(type, requestUrl, method, strParams, json));

        return {
          ok: true,
          data: json
        };
      })
      .catch(e => {
        /* eslint-disable */
        console.error(e, requestUrl, type);
        /* eslint-enable */

        return {
          ok: false,
          data: {
            message: e
          }
        };
      });
  };
};

const get = (
  url: string | Function,
  type: string,
  params: Object = {},
  headers: Object = {}
) => request(url, "GET", params, headers, type);
const post = (
  url: string | Function,
  type: string,
  params: Object = {},
  headers: Object = {}
) => request(url, "POST", params, headers, type);
const del = (
  url: string | Function,
  type: string,
  params: Object = {},
  headers: Object = {}
) => request(url, "DELETE", params, headers, type);
const put = (
  url: string | Function,
  type: string,
  params: Object = {},
  headers: Object = {}
) => request(url, "PUT", params, headers, type);
const head = (
  url: string | Function,
  type: string,
  params: Object = {},
  headers: Object = {}
) => request(url, "HEAD", params, headers, type);

module.exports = {
  get,
  post,
  put,
  del,
  head
};
