import AuthFactory from "./Auth";
import server from "./server";
import {
  getFilledCollection,
  getUrlNextPage,
  editCollection
} from "./reducers/componentHelpers";
import { reducer, initialStateBase } from "./reducers/reducer";
import simpleModelReducer, { urls } from "./reducers/simpleModel";
import { addParamsToUrl } from "./helpers/url";

let Auth = AuthFactory();

export {
  Auth,
  server,
  reducer,
  getFilledCollection,
  getUrlNextPage,
  editCollection,
  initialStateBase,
  simpleModelReducer,
  addParamsToUrl,
  urls
};
