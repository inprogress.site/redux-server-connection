// @flow

import httpStatusCodes from "http-status-codes";
import CookieManager from "./helpers/CookieManager";

class Auth {
  user: Object | null;
  endpoints: Object;

  constructor() {
    this.user = null;
    this.endpoints = {
      login: "/api/auth",
      logout: "/api/auth/logout",
      me: "/api/v1/users/me"
    };
  }

  setEndpoints(endpoints: Object) {
    this.endpoints = endpoints;
  }

  setUser(user: Object | null, save: boolean = true) {
    if (
      typeof navigator !== "undefined" &&
      navigator.product === "ReactNative"
    ) {
      import("react-native").then(native => {
        let { AsyncStorage } = native;

        if (save) {
          if (user) {
            AsyncStorage.setItem("user", JSON.stringify(user));
          } else {
            AsyncStorage.removeItem("user");
          }
        }
      });
    }

    this.user = user;
  }

  getToken() {
    if (this.user) {
      return this.user.token;
    }

    return "";
  }

  login(credentials: Object, callback: Function = () => {}) {
    let res = {
      data: null,
      status: null,
      error: null
    };
    const options = {
      method: "POST",
      body: JSON.stringify(credentials),
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(this.endpoints.login, options)
      .then(response => {
        if (response.status !== httpStatusCodes.OK) {
          return response.json().then(json => {
            throw new Error(json.errors ? json.errors.message : json.message);
          });
        }
        return response.json();
      })
      .then(json => {
        fetch(this.endpoints.me, {
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "x-token": json.access_token
          }
        })
          .then(response => {
            if (response.status !== httpStatusCodes.OK) {
              return response.json().then(json => {
                throw new Error(
                  json.errors ? json.errors.message : json.message
                );
              });
            }
            return response.json();
          })
          .then(userJson => {
            let user = userJson.data;
            user.token = json.access_token;
            this.setUser(user);
            res = {
              status: httpStatusCodes.OK,
              data: userJson
            };
            return callback(res);
          });
      })
      .catch(err => {
        this.logout();
        res = {
          status: httpStatusCodes.UNAUTHORIZED,
          error: err.message
        };
        callback(res);
      });
  }

  logout(callback: Function = () => {}) {
    let res = {
      data: null,
      status: null,
      error: null
    };

    fetch(this.endpoints.logout, { credentials: "same-origin" })
      .then(response => {
        if (response.status !== httpStatusCodes.OK) {
          return response.json().then(json => {
            throw new Error(json.errors ? json.errors.message : json.message);
          });
        }
        return response.json();
      })
      .then(json => {
        this.setUser(null);
        res = {
          status: httpStatusCodes.OK,
          data: json
        };
        return callback(res);
      })
      .catch(err => {
        this.setUser(null);
        res = {
          status: httpStatusCodes.UNAUTHORIZED,
          error: err.message
        };
        callback(res);
      });
  }

  isLoggedIn() {
    return !!this.getUser();
  }

  requiresAuth(state: Object, replace: Function) {
    if (!this.isLoggedIn()) {
      replace({
        pathname: "/login",
        query: {
          to: state.location.pathname
        }
      });
    }
  }

  getUser() {
    if (
      typeof document === "undefined" ||
      (typeof navigator !== "undefined" && navigator.product === "ReactNative")
    ) {
      return this.user;
    }

    try {
      return JSON.parse(decodeURIComponent(this.getCookie("user")));
    } catch (e) {
      return null;
    }
  }

  getCookie(name: string): string {
    return CookieManager.readCookie(name);
  }

  setCookie(name: string, value: Object | string) {
    try {
      if (typeof value === "object") {
        value = encodeURIComponent(JSON.stringify(value));
      }
      CookieManager.createCookie(name, value, 1);
    } catch (e) {
      throw e;
    }
  }
}

let instance;

export default () => {
  if (!instance) {
    instance = new Auth();

    if (
      typeof navigator !== "undefined" &&
      navigator.product === "ReactNative"
    ) {
      import("react-native").then(native => {
        let { AsyncStorage } = native;

        AsyncStorage.getItem("user")
          .then(user => instance.setUser(JSON.parse(user), false))
          .catch(e => {});
      });
    }
  }

  return instance;
};
