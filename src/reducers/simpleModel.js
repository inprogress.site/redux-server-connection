// @flow

import { reducer, initialStateBase } from "./reducer";

interface urlOpts {
  name: string;
  singleActions: Array<string | { name: string, action: string }>;
  actions: Array<string | { name: string, action: string }>;
  children: urlOpts;
  hostname: string;
  version: number;
}

const urlBuilder = (
  baseUrl: string | (() => string),
  singleActions: Array<string | { name: string, action: string }> = [],
  actions: Array<string | { name: string, action: string }> = [],
  level: number = 0
) => {
  let base = {};

  if (typeof baseUrl === "string") {
    base = {
      list: `${baseUrl}`,
      single: (id: string) => `${baseUrl}/${id}`
    };
  } else {
    base = {
      list: idParent => `${baseUrl(idParent)}`,
      single: (idParent: string, id: string) => `${baseUrl(idParent)}/${id}`
    };
  }

  singleActions.forEach(action => {
    let index = typeof action === "string" ? action : action.name;
    let urlAction = typeof action === "string" ? action : action.action;

    if (typeof baseUrl === "string") {
      return (base[index] = (id: string) => `${baseUrl}/${id}/${urlAction}`);
    }

    return (base[index] = (idParent: string, id: string) =>
      `${baseUrl(idParent)}/${id}/${urlAction}`);
  });

  actions.forEach(action => {
    let index = typeof action === "string" ? action : action.name;
    let urlAction = typeof action === "string" ? action : action.action;

    if (typeof baseUrl === "string") {
      return (base[index] = `${baseUrl}/${urlAction}`);
    }

    return (base[index] = (idParent: string) =>
      `${baseUrl(idParent)}/${urlAction}`);
  });

  return base;
};

export const urls = (opts: urlOpts) => {
  let version = opts.version || 1;
  let hostname = opts.hostname || "";

  let baseUrl = `${hostname}/api/v${version}/${opts.name}`;

  let base = urlBuilder(baseUrl, opts.singleActions, opts.actions);

  if (opts.children) {
    base[opts.children.name] = urlBuilder(
      (id: string) => `${baseUrl}/${id}/${opts.children.name}`,
      opts.children.singleActions,
      opts.children.actions
    );
  }

  return base;
};

const initialState = (name, metadataKey = ""): Object => {
  return Object.assign({}, initialStateBase, {
    metadataKey: metadataKey
  });
};

export default (name: string, metadataKey: string | Array<string>) => {
  return (
    state: Object = initialState(name, metadataKey || name),
    action: { type: string, url: string, data: any }
  ) => {
    switch (action.type) {
      default:
        return reducer(state, action, name.toUpperCase());
    }
  };
};
