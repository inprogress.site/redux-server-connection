// @flow

import { addParamsToUrl } from "../helpers/url";
import { stringify } from "qs";

export const getFilledCollection = (
  reducerState: Object,
  url: string,
  params?: Object = null
) => {
  const urlWithParams = addParamsToUrl(url.replace(/.*\/\/[^\/]*/, ""), params);

  const collection = reducerState.collections[urlWithParams];
  const collectionMetadata = reducerState.collectionMetadata[urlWithParams];

  if (!Array.isArray(collection)) {
    return [];
  }

  const result = collection
    .map(id => reducerState.elements[id] || null)
    .filter(element => element !== null);

  if (collectionMetadata) {
    result.total = collectionMetadata.total;
    result.page = collectionMetadata.page;
  }

  return result;
};

export const editCollection = (
  reducer: string,
  url: string,
  addRemove: string,
  id: number
) => {
  return {
    type: `EDIT_COLLECTION_${addRemove}_${reducer}`,
    data: {
      id,
      url
    }
  };
};

export const getUrlNextPage = (
  reducerState: {
    collections: { [url: string]: Array<number> },
    collectionMetadata: { [url: string]: Object }
  },
  url: string,
  params?: Object
) => {
  let urlWithParams = url.replace(/.*\/\/[^\/]*/, "");

  if (params !== null) {
    urlWithParams += `?${stringify(params)}`;
  }

  if (
    !reducerState.collections[urlWithParams] ||
    !reducerState.collectionMetadata[urlWithParams] ||
    !reducerState.collectionMetadata[urlWithParams].page
  ) {
    return urlWithParams;
  }

  const query = Object.assign({}, params, {
    page: parseInt(reducerState.collectionMetadata[urlWithParams].page, 10) + 1
  });
  const finalParams = Object.assign({}, params, { query });

  return `${url.replace(/.*\/\/[^\/]*/, "")}?${stringify(finalParams)}`;
};
