// @flow

export const initialStateBase = {
  elements: {},
  collections: {},
  isFetching: {},
  collectionMetadata: {}
};

const getNewElements = (state, array) => {
  if (!Array.isArray(array) || array.length === 0) {
    return state.elements;
  }

  const newElements = {};

  array.forEach(element => {
    newElements[element.id] = element;
  });

  return Object.assign({}, state.elements, newElements);
};

const getNewCollections = (state, url, data, page, alwaysAppend = false) => {
  const newCollections = {};

  if (data) {
    if (Array.isArray(data.data) && data.data.length && data.data[0].id) {
      const collection = data.data.map(element => element.id);

      if (
        Array.isArray(state.collections[url]) &&
        (alwaysAppend ||
          (state.collectionMetadata[url] &&
            page == state.collectionMetadata[url].page))
      ) {
        newCollections[url] = [].concat(state.collections[url], collection);
        newCollections[url] = newCollections[url].filter(
          (id, i, self) => self.indexOf(id) === i
        );
      } else {
        newCollections[url] = collection;
      }
    } else if (alwaysAppend) {
      newCollections[url] = state.collections[url];
    } else {
      newCollections[url] = data.data ? data.data : data;
    }
  }

  return Object.assign({}, state.collections, newCollections);
};

const getNewMetadata = (state, url, data) => {
  const newMetadata = {};

  if (data && data.meta) {
    newMetadata[url] = {
      page: data.meta.page,
      total: data.meta.total
    };
  }

  return Object.assign({}, state.collectionMetadata, newMetadata);
};

const changeFetchingStatus = (state, action, status) => {
  const isFetching = {};

  isFetching[action.url.replace(/(%2C)?%22page%22%3A%22.*%22/g, "")] = status;

  return {
    isFetching: Object.assign({}, state.isFetching, isFetching)
  };
};

export function reducer(
  state: {
    metadataKey: string | Array<string>,
    alwaysAppend: boolean,
    elements: { [id: string]: Object },
    collections: { [url: string]: Array<number> },
    isFetching: { [url: string]: boolean },
    collectionMetadata: { [url: string]: Object }
  },
  action: { type: string, url: string, data: any },
  type: string
) {
  let newState;

  switch (action.type) {
    case `REQUEST_${type.toUpperCase()}`: {
      newState = Object.assign(
        {},
        state,
        changeFetchingStatus(state, action, true)
      );
      break;
    }
    case `RECEIVE_${type.toUpperCase()}`: {
      const match = /"page":"(.*)"/g.exec(decodeURIComponent(action.url));
      const page = match ? match[1] : null;

      action.url = action.url.replace(/(%2C)?%22page%22%3A%22.*%22/g, "");

      const newElements = Object.assign(
        {},
        state.elements,
        getNewElements(state, action.data && action.data.data)
      );

      newState = Object.assign(
        {},
        state,
        {
          elements: newElements,
          collections: getNewCollections(
            state,
            action.url,
            action.data,
            page,
            state.alwaysAppend
          ),
          collectionMetadata: getNewMetadata(state, action.url, action.data)
        },
        changeFetchingStatus(state, action, false)
      );
      break;
    }
    case `RESPONSE_PUT_${type.toUpperCase()}`:
    case `RESPONSE_POST_${type.toUpperCase()}`: {
      const match = /page=(.*)/g.exec(decodeURIComponent(action.url));
      const page = match ? match[1] : null;

      newState = Object.assign(
        {},
        state,
        {
          collections: getNewCollections(state, action.url, action.data, page),
          collectionMetadata: getNewMetadata(state, action.url, action.data)
        },
        changeFetchingStatus(state, action, false)
      );
      break;
    }
    case `RECEIVE_ELEMENT_${type.toUpperCase()}`:
    case `RESPONSE_PATCH_ELEMENT_${type.toUpperCase()}`:
    case `RESPONSE_PUT_ELEMENT_${type.toUpperCase()}`:
    case `RESPONSE_POST_ELEMENT_${type.toUpperCase()}`: {
      const newElements = Object.assign(
        {},
        state.elements,
        getNewElements(state, [action.data.data])
      );

      newState = Object.assign(
        {},
        state,
        {
          elements: newElements
        },
        changeFetchingStatus(state, action, false)
      );
      break;
    }
    case `RESPONSE_DELETE_${type.toUpperCase()}`: {
      newState = Object.assign(
        {},
        state,
        changeFetchingStatus(state, action, false)
      );
      break;
    }
    case `EDIT_COLLECTION_REPLACE_${type.toUpperCase()}`: {
      let newCollection = {};
      newCollection[action.data.url] = action.data.id;

      newState = Object.assign({}, state, {
        collections: Object.assign({}, state.collections, newCollection)
      });
      break;
    }
    case `EDIT_COLLECTION_ADD_${type.toUpperCase()}`: {
      let newCollection = {};
      if (state.collections[action.data.url]) {
        newCollection[action.data.url] = state.collections[
          action.data.url
        ].push(action.data.id);
      } else {
        newCollection[action.data.url] = [action.data.id];
      }

      newState = Object.assign({}, state, {
        collections: Object.assign({}, state.collections, newCollection)
      });
      break;
    }
    case `EDIT_COLLECTION_REMOVE_${type.toUpperCase()}`: {
      let newCollection = {};
      newCollection[action.data.url] = state.collections[
        action.data.url
      ].filter(element => element !== action.data.id);

      newState = Object.assign({}, state, {
        collections: Object.assign({}, state.collections, newCollection)
      });
      break;
    }
    default: {
      newState = state;
    }
  }

  if (
    state.metadataKey &&
    (/RECEIVE_[A-Z_]*/gi.test(action.type) ||
      /RESPONSE_[A-Z_]*/gi.test(action.type)) &&
    action.data &&
    action.data.meta &&
    typeof action.data.meta[state.metadataKey] !== "undefined"
  ) {
    const metadataArray = Object.keys(action.data.meta[state.metadataKey]).map(
      id => action.data.meta[state.metadataKey][id]
    );
    const newElements = Object.assign(
      {},
      newState.elements,
      getNewElements(state, metadataArray)
    );

    return Object.assign({}, newState, { elements: newElements });
  }

  return newState;
}
