// @flow

import { stringify } from "qs";

export function getUrlParams(url: string) {
  const urlSplitted = url.split("/").slice(1);

  return urlSplitted.filter(el => {
    return !Number.isNaN(Number.parseInt(el, 10));
  });
}

export function addParamsToUrl(url: string, params: Object = null) {
  let urlWithParams = url;

  const qs = stringify(params);

  if (qs.length) {
    urlWithParams += `?${qs}`;
  }

  return urlWithParams;
}
