// @flow

const expiryInMiliSeconds = 86400000;

export default class CookieManager {
  static createCookie(name: string, value: string, days: number) {
    let expires;

    if (days) {
      const date = new Date();

      date.setTime(date.getTime() + days * expiryInMiliSeconds);
      expires = "; expires=".concat(date.toGMTString());
    } else {
      expires = "";
    }
    document.cookie = name.concat("=", value, expires, "; path=/");
  }

  static readCookie(name: string): string {
    const nameEQ = name.concat("=");
    const ca = document.cookie.split(";");

    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];

      while (c.charAt(0) === " ") {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }

  static eraseCookie(name) {
    this.createCookie(name, "", -1);
  }
}
