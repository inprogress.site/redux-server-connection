// @flow

import httpStatusCodes from "http-status-codes";

function isInformational(status: number) {
  return status >= httpStatusCodes.CONTINUE && status < httpStatusCodes.OK;
}

function isSuccess(status: number) {
  return status >= httpStatusCodes.OK &&
    status < httpStatusCodes.MULTIPLE_CHOICES;
}

function isRedirection(status: number) {
  return status >= httpStatusCodes.MULTIPLE_CHOICES &&
    status < httpStatusCodes.BAD_REQUEST;
}

function isClientError(status: number) {
  return status >= httpStatusCodes.BAD_REQUEST &&
    status < httpStatusCodes.INTERNAL_SERVER_ERROR;
}

function isServerError(status: number) {
  return status >= httpStatusCodes.INTERNAL_SERVER_ERROR;
}

function isError(status: number) {
  return status >= httpStatusCodes.BAD_REQUEST;
}

export default {
  isInformational,
  isSuccess,
  isRedirection,
  isClientError,
  isServerError,
  isError
};
