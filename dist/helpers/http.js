"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _httpStatusCodes = require("http-status-codes");

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isInformational(status) {
  return status >= _httpStatusCodes2.default.CONTINUE && status < _httpStatusCodes2.default.OK;
}

function isSuccess(status) {
  return status >= _httpStatusCodes2.default.OK && status < _httpStatusCodes2.default.MULTIPLE_CHOICES;
}

function isRedirection(status) {
  return status >= _httpStatusCodes2.default.MULTIPLE_CHOICES && status < _httpStatusCodes2.default.BAD_REQUEST;
}

function isClientError(status) {
  return status >= _httpStatusCodes2.default.BAD_REQUEST && status < _httpStatusCodes2.default.INTERNAL_SERVER_ERROR;
}

function isServerError(status) {
  return status >= _httpStatusCodes2.default.INTERNAL_SERVER_ERROR;
}

function isError(status) {
  return status >= _httpStatusCodes2.default.BAD_REQUEST;
}

exports.default = {
  isInformational: isInformational,
  isSuccess: isSuccess,
  isRedirection: isRedirection,
  isClientError: isClientError,
  isServerError: isServerError,
  isError: isError
};