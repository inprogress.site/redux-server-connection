"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUrlParams = getUrlParams;
exports.addParamsToUrl = addParamsToUrl;

var _qs = require("qs");

function getUrlParams(url) {
  var urlSplitted = url.split("/").slice(1);

  return urlSplitted.filter(function (el) {
    return !Number.isNaN(Number.parseInt(el, 10));
  });
}

function addParamsToUrl(url) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  var urlWithParams = url;

  var qs = (0, _qs.stringify)(params);

  if (qs.length) {
    urlWithParams += "?" + qs;
  }

  return urlWithParams;
}