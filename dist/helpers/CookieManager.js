"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var expiryInMiliSeconds = 86400000;

var CookieManager = function () {
  function CookieManager() {
    _classCallCheck(this, CookieManager);
  }

  _createClass(CookieManager, null, [{
    key: "createCookie",
    value: function createCookie(name, value, days) {
      var expires = void 0;

      if (days) {
        var date = new Date();

        date.setTime(date.getTime() + days * expiryInMiliSeconds);
        expires = "; expires=".concat(date.toGMTString());
      } else {
        expires = "";
      }
      document.cookie = name.concat("=", value, expires, "; path=/");
    }
  }, {
    key: "readCookie",
    value: function readCookie(name) {
      var nameEQ = name.concat("=");
      var ca = document.cookie.split(";");

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) === " ") {
          c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
          return c.substring(nameEQ.length, c.length);
        }
      }
      return null;
    }
  }, {
    key: "eraseCookie",
    value: function eraseCookie(name) {
      this.createCookie(name, "", -1);
    }
  }]);

  return CookieManager;
}();

exports.default = CookieManager;