"use strict";

var _Auth = require("./Auth");

var _Auth2 = _interopRequireDefault(_Auth);

var _httpStatusCodes = require("http-status-codes");

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

var _http = require("./helpers/http");

var _http2 = _interopRequireDefault(_http);

var _qs = require("qs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Auth = (0, _Auth2.default)();
/* global FormData */

var requested = function requested(type, url) {
  return {
    type: "REQUEST_" + type.toUpperCase(),
    url: url.replace(/.*\/\/[^\/]*/, "")
  };
};

var received = function received(type, url, method, params, data) {
  var response = {
    data: data,
    url: url.replace(/.*\/\/[^\/]*/, ""),
    params: params
  };

  switch (method.toLowerCase()) {
    case "get":
    case "head":
      response.type = "RECEIVE_" + type.toUpperCase();
      break;
    default:
      response.type = "RESPONSE_" + method.toUpperCase() + "_" + type.toUpperCase();
      break;
  }
  return response;
};

var request = function request(url, method) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var type = arguments[4];

  if (url === "") {
    return function () {};
  }

  var strParams = (0, _qs.stringify)(params);
  var body = params;
  var init = { method: method, credentials: "same-origin" };

  if (typeof FormData === "undefined" || !(params instanceof FormData)) {
    body = JSON.stringify(params);
  }

  if (method !== "GET" && method !== "HEAD") {
    init.body = body;
    if (typeof FormData === "undefined" || !(body instanceof FormData)) {
      headers = Object.assign(headers, {
        Accept: "application/json",
        "Content-Type": "application/json"
      });
    }
  }

  init.headers = headers;
  if (typeof navigator !== "undefined" && navigator.product === "ReactNative") {
    init.headers["x-token"] = Auth.getToken();
  }

  return function (dispatch, getState) {
    var requestUrl = void 0;

    if (typeof url === "function") {
      requestUrl = url(getState());
    } else {
      requestUrl = url;
    }

    if (strParams !== "" && (method === "GET" || method === "HEAD")) {
      requestUrl += "?" + strParams;
    }

    dispatch(requested(type, requestUrl));
    return fetch(requestUrl, init).then(function (response) {
      if (response.status === _httpStatusCodes2.default.UNAUTHORIZED) {
        Auth.logout();
      }

      if (method === "HEAD" || method === "DELETE") {
        return _http2.default.isSuccess(response.status);
      } else if (method === "GET") {
        if (response.status === _httpStatusCodes2.default.NO_CONTENT || response.status === _httpStatusCodes2.default.ACCEPTED) {
          return true;
        } else if (response.status === _httpStatusCodes2.default.NOT_FOUND) {
          return false;
        }
      }

      if (!_http2.default.isSuccess(response.status)) {
        throw new Error(response.statusText);
      }

      return response.json();
    }).then(function (json) {
      dispatch(received(type, requestUrl, method, strParams, json));

      return {
        ok: true,
        data: json
      };
    }).catch(function (e) {
      /* eslint-disable */
      console.error(e, requestUrl, type);
      /* eslint-enable */

      return {
        ok: false,
        data: {
          message: e
        }
      };
    });
  };
};

var get = function get(url, type) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return request(url, "GET", params, headers, type);
};
var post = function post(url, type) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return request(url, "POST", params, headers, type);
};
var del = function del(url, type) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return request(url, "DELETE", params, headers, type);
};
var put = function put(url, type) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return request(url, "PUT", params, headers, type);
};
var head = function head(url, type) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return request(url, "HEAD", params, headers, type);
};

module.exports = {
  get: get,
  post: post,
  put: put,
  del: del,
  head: head
};