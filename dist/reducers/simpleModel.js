"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.urls = undefined;

var _reducer = require("./reducer");

var urlBuilder = function urlBuilder(baseUrl) {
  var singleActions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var actions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var level = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

  var base = {};

  if (typeof baseUrl === "string") {
    base = {
      list: "" + baseUrl,
      single: function single(id) {
        return baseUrl + "/" + id;
      }
    };
  } else {
    base = {
      list: function list(idParent) {
        return "" + baseUrl(idParent);
      },
      single: function single(idParent, id) {
        return baseUrl(idParent) + "/" + id;
      }
    };
  }

  singleActions.forEach(function (action) {
    var index = typeof action === "string" ? action : action.name;
    var urlAction = typeof action === "string" ? action : action.action;

    if (typeof baseUrl === "string") {
      return base[index] = function (id) {
        return baseUrl + "/" + id + "/" + urlAction;
      };
    }

    return base[index] = function (idParent, id) {
      return baseUrl(idParent) + "/" + id + "/" + urlAction;
    };
  });

  actions.forEach(function (action) {
    var index = typeof action === "string" ? action : action.name;
    var urlAction = typeof action === "string" ? action : action.action;

    if (typeof baseUrl === "string") {
      return base[index] = baseUrl + "/" + urlAction;
    }

    return base[index] = function (idParent) {
      return baseUrl(idParent) + "/" + urlAction;
    };
  });

  return base;
};

var urls = exports.urls = function urls(opts) {
  var version = opts.version || 1;
  var hostname = opts.hostname || "";

  var baseUrl = hostname + "/api/v" + version + "/" + opts.name;

  var base = urlBuilder(baseUrl, opts.singleActions, opts.actions);

  if (opts.children) {
    base[opts.children.name] = urlBuilder(function (id) {
      return baseUrl + "/" + id + "/" + opts.children.name;
    }, opts.children.singleActions, opts.children.actions);
  }

  return base;
};

var initialState = function initialState(name) {
  var metadataKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";

  return Object.assign({}, _reducer.initialStateBase, {
    metadataKey: metadataKey
  });
};

exports.default = function (name, metadataKey) {
  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState(name, metadataKey || name);
    var action = arguments[1];

    switch (action.type) {
      default:
        return (0, _reducer.reducer)(state, action, name.toUpperCase());
    }
  };
};