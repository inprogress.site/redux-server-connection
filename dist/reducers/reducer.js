"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reducer = reducer;
var initialStateBase = exports.initialStateBase = {
  elements: {},
  collections: {},
  isFetching: {},
  collectionMetadata: {}
};

var getNewElements = function getNewElements(state, array) {
  if (!Array.isArray(array) || array.length === 0) {
    return state.elements;
  }

  var newElements = {};

  array.forEach(function (element) {
    newElements[element.id] = element;
  });

  return Object.assign({}, state.elements, newElements);
};

var getNewCollections = function getNewCollections(state, url, data, page) {
  var alwaysAppend = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

  var newCollections = {};

  if (data) {
    if (Array.isArray(data.data) && data.data.length && data.data[0].id) {
      var collection = data.data.map(function (element) {
        return element.id;
      });

      if (Array.isArray(state.collections[url]) && (alwaysAppend || state.collectionMetadata[url] && page == state.collectionMetadata[url].page)) {
        newCollections[url] = [].concat(state.collections[url], collection);
        newCollections[url] = newCollections[url].filter(function (id, i, self) {
          return self.indexOf(id) === i;
        });
      } else {
        newCollections[url] = collection;
      }
    } else if (alwaysAppend) {
      newCollections[url] = state.collections[url];
    } else {
      newCollections[url] = data.data ? data.data : data;
    }
  }

  return Object.assign({}, state.collections, newCollections);
};

var getNewMetadata = function getNewMetadata(state, url, data) {
  var newMetadata = {};

  if (data && data.meta) {
    newMetadata[url] = {
      page: data.meta.page,
      total: data.meta.total
    };
  }

  return Object.assign({}, state.collectionMetadata, newMetadata);
};

var changeFetchingStatus = function changeFetchingStatus(state, action, status) {
  var isFetching = {};

  isFetching[action.url.replace(/(%2C)?%22page%22%3A%22.*%22/g, "")] = status;

  return {
    isFetching: Object.assign({}, state.isFetching, isFetching)
  };
};

function reducer(state, action, type) {
  var newState = void 0;

  switch (action.type) {
    case "REQUEST_" + type.toUpperCase():
      {
        newState = Object.assign({}, state, changeFetchingStatus(state, action, true));
        break;
      }
    case "RECEIVE_" + type.toUpperCase():
      {
        var match = /"page":"(.*)"/g.exec(decodeURIComponent(action.url));
        var page = match ? match[1] : null;

        action.url = action.url.replace(/(%2C)?%22page%22%3A%22.*%22/g, "");

        var newElements = Object.assign({}, state.elements, getNewElements(state, action.data && action.data.data));

        newState = Object.assign({}, state, {
          elements: newElements,
          collections: getNewCollections(state, action.url, action.data, page, state.alwaysAppend),
          collectionMetadata: getNewMetadata(state, action.url, action.data)
        }, changeFetchingStatus(state, action, false));
        break;
      }
    case "RESPONSE_PUT_" + type.toUpperCase():
    case "RESPONSE_POST_" + type.toUpperCase():
      {
        var _match = /page=(.*)/g.exec(decodeURIComponent(action.url));
        var _page = _match ? _match[1] : null;

        newState = Object.assign({}, state, {
          collections: getNewCollections(state, action.url, action.data, _page),
          collectionMetadata: getNewMetadata(state, action.url, action.data)
        }, changeFetchingStatus(state, action, false));
        break;
      }
    case "RECEIVE_ELEMENT_" + type.toUpperCase():
    case "RESPONSE_PATCH_ELEMENT_" + type.toUpperCase():
    case "RESPONSE_PUT_ELEMENT_" + type.toUpperCase():
    case "RESPONSE_POST_ELEMENT_" + type.toUpperCase():
      {
        var _newElements = Object.assign({}, state.elements, getNewElements(state, [action.data.data]));

        newState = Object.assign({}, state, {
          elements: _newElements
        }, changeFetchingStatus(state, action, false));
        break;
      }
    case "RESPONSE_DELETE_" + type.toUpperCase():
      {
        newState = Object.assign({}, state, changeFetchingStatus(state, action, false));
        break;
      }
    case "EDIT_COLLECTION_REPLACE_" + type.toUpperCase():
      {
        var newCollection = {};
        newCollection[action.data.url] = action.data.id;

        newState = Object.assign({}, state, {
          collections: Object.assign({}, state.collections, newCollection)
        });
        break;
      }
    case "EDIT_COLLECTION_ADD_" + type.toUpperCase():
      {
        var _newCollection = {};
        if (state.collections[action.data.url]) {
          _newCollection[action.data.url] = state.collections[action.data.url].push(action.data.id);
        } else {
          _newCollection[action.data.url] = [action.data.id];
        }

        newState = Object.assign({}, state, {
          collections: Object.assign({}, state.collections, _newCollection)
        });
        break;
      }
    case "EDIT_COLLECTION_REMOVE_" + type.toUpperCase():
      {
        var _newCollection2 = {};
        _newCollection2[action.data.url] = state.collections[action.data.url].filter(function (element) {
          return element !== action.data.id;
        });

        newState = Object.assign({}, state, {
          collections: Object.assign({}, state.collections, _newCollection2)
        });
        break;
      }
    default:
      {
        newState = state;
      }
  }

  if (state.metadataKey && (/RECEIVE_[A-Z_]*/gi.test(action.type) || /RESPONSE_[A-Z_]*/gi.test(action.type)) && action.data && action.data.meta && typeof action.data.meta[state.metadataKey] !== "undefined") {
    var metadataArray = Object.keys(action.data.meta[state.metadataKey]).map(function (id) {
      return action.data.meta[state.metadataKey][id];
    });
    var _newElements2 = Object.assign({}, newState.elements, getNewElements(state, metadataArray));

    return Object.assign({}, newState, { elements: _newElements2 });
  }

  return newState;
}