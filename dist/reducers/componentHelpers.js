"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUrlNextPage = exports.editCollection = exports.getFilledCollection = undefined;

var _url = require("../helpers/url");

var _qs = require("qs");

var getFilledCollection = exports.getFilledCollection = function getFilledCollection(reducerState, url) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  var urlWithParams = (0, _url.addParamsToUrl)(url.replace(/.*\/\/[^\/]*/, ""), params);

  var collection = reducerState.collections[urlWithParams];
  var collectionMetadata = reducerState.collectionMetadata[urlWithParams];

  if (!Array.isArray(collection)) {
    return [];
  }

  var result = collection.map(function (id) {
    return reducerState.elements[id] || null;
  }).filter(function (element) {
    return element !== null;
  });

  if (collectionMetadata) {
    result.total = collectionMetadata.total;
    result.page = collectionMetadata.page;
  }

  return result;
};

var editCollection = exports.editCollection = function editCollection(reducer, url, addRemove, id) {
  return {
    type: "EDIT_COLLECTION_" + addRemove + "_" + reducer,
    data: {
      id: id,
      url: url
    }
  };
};

var getUrlNextPage = exports.getUrlNextPage = function getUrlNextPage(reducerState, url, params) {
  var urlWithParams = url.replace(/.*\/\/[^\/]*/, "");

  if (params !== null) {
    urlWithParams += "?" + (0, _qs.stringify)(params);
  }

  if (!reducerState.collections[urlWithParams] || !reducerState.collectionMetadata[urlWithParams] || !reducerState.collectionMetadata[urlWithParams].page) {
    return urlWithParams;
  }

  var query = Object.assign({}, params, {
    page: parseInt(reducerState.collectionMetadata[urlWithParams].page, 10) + 1
  });
  var finalParams = Object.assign({}, params, { query: query });

  return url.replace(/.*\/\/[^\/]*/, "") + "?" + (0, _qs.stringify)(finalParams);
};