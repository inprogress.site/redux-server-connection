"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _httpStatusCodes = require("http-status-codes");

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

var _CookieManager = require("./helpers/CookieManager");

var _CookieManager2 = _interopRequireDefault(_CookieManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Auth = function () {
  function Auth() {
    _classCallCheck(this, Auth);

    this.user = null;
    this.endpoints = {
      login: "/api/auth",
      logout: "/api/auth/logout",
      me: "/api/v1/users/me"
    };
  }

  _createClass(Auth, [{
    key: "setEndpoints",
    value: function setEndpoints(endpoints) {
      this.endpoints = endpoints;
    }
  }, {
    key: "setUser",
    value: function setUser(user) {
      var save = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      if (typeof navigator !== "undefined" && navigator.product === "ReactNative") {
        Promise.resolve().then(function () {
          return require("react-native");
        }).then(function (native) {
          var AsyncStorage = native.AsyncStorage;


          if (save) {
            if (user) {
              AsyncStorage.setItem("user", JSON.stringify(user));
            } else {
              AsyncStorage.removeItem("user");
            }
          }
        });
      }

      this.user = user;
    }
  }, {
    key: "getToken",
    value: function getToken() {
      if (this.user) {
        return this.user.token;
      }

      return "";
    }
  }, {
    key: "login",
    value: function login(credentials) {
      var _this = this;

      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};

      var res = {
        data: null,
        status: null,
        error: null
      };
      var options = {
        method: "POST",
        body: JSON.stringify(credentials),
        credentials: "same-origin",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      };

      fetch(this.endpoints.login, options).then(function (response) {
        if (response.status !== _httpStatusCodes2.default.OK) {
          return response.json().then(function (json) {
            throw new Error(json.errors ? json.errors.message : json.message);
          });
        }
        return response.json();
      }).then(function (json) {
        fetch(_this.endpoints.me, {
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "x-token": json.access_token
          }
        }).then(function (response) {
          if (response.status !== _httpStatusCodes2.default.OK) {
            return response.json().then(function (json) {
              throw new Error(json.errors ? json.errors.message : json.message);
            });
          }
          return response.json();
        }).then(function (userJson) {
          var user = userJson.data;
          user.token = json.access_token;
          _this.setUser(user);
          res = {
            status: _httpStatusCodes2.default.OK,
            data: userJson
          };
          return callback(res);
        });
      }).catch(function (err) {
        _this.logout();
        res = {
          status: _httpStatusCodes2.default.UNAUTHORIZED,
          error: err.message
        };
        callback(res);
      });
    }
  }, {
    key: "logout",
    value: function logout() {
      var _this2 = this;

      var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};

      var res = {
        data: null,
        status: null,
        error: null
      };

      fetch(this.endpoints.logout, { credentials: "same-origin" }).then(function (response) {
        if (response.status !== _httpStatusCodes2.default.OK) {
          return response.json().then(function (json) {
            throw new Error(json.errors ? json.errors.message : json.message);
          });
        }
        return response.json();
      }).then(function (json) {
        _this2.setUser(null);
        res = {
          status: _httpStatusCodes2.default.OK,
          data: json
        };
        return callback(res);
      }).catch(function (err) {
        _this2.setUser(null);
        res = {
          status: _httpStatusCodes2.default.UNAUTHORIZED,
          error: err.message
        };
        callback(res);
      });
    }
  }, {
    key: "isLoggedIn",
    value: function isLoggedIn() {
      return !!this.getUser();
    }
  }, {
    key: "requiresAuth",
    value: function requiresAuth(state, replace) {
      if (!this.isLoggedIn()) {
        replace({
          pathname: "/login",
          query: {
            to: state.location.pathname
          }
        });
      }
    }
  }, {
    key: "getUser",
    value: function getUser() {
      if (typeof document === "undefined" || typeof navigator !== "undefined" && navigator.product === "ReactNative") {
        return this.user;
      }

      try {
        return JSON.parse(decodeURIComponent(this.getCookie("user")));
      } catch (e) {
        return null;
      }
    }
  }, {
    key: "getCookie",
    value: function getCookie(name) {
      return _CookieManager2.default.readCookie(name);
    }
  }, {
    key: "setCookie",
    value: function setCookie(name, value) {
      try {
        if ((typeof value === "undefined" ? "undefined" : _typeof(value)) === "object") {
          value = encodeURIComponent(JSON.stringify(value));
        }
        _CookieManager2.default.createCookie(name, value, 1);
      } catch (e) {
        throw e;
      }
    }
  }]);

  return Auth;
}();

var instance = void 0;

exports.default = function () {
  if (!instance) {
    instance = new Auth();

    if (typeof navigator !== "undefined" && navigator.product === "ReactNative") {
      Promise.resolve().then(function () {
        return require("react-native");
      }).then(function (native) {
        var AsyncStorage = native.AsyncStorage;


        AsyncStorage.getItem("user").then(function (user) {
          return instance.setUser(JSON.parse(user), false);
        }).catch(function (e) {});
      });
    }
  }

  return instance;
};